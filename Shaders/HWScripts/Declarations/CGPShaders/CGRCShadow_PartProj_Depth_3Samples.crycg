////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCShadow_Depth2_1Samples.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      PS20Only
      NoFog

      MainInput { uniform sampler2D shadMap0 : register(s0),
                  uniform sampler2D shadMap1 : register(s1),
                  uniform sampler2D shadMap2 : register(s2),
                  uniform sampler2D baseMap : register(s3),
                  uniform samplerCUBE projMap : register(s4),
                  uniform float4 Ambient,
                  uniform float4 Diffuse,
                  uniform float4 Fading }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_T4
          OUT_T5
          OUT_T6
          OUT_C0
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        FLOAT4 decalColor = tex2D(baseMap, IN.Tex3.xy);
        FLOAT4 projColor = texCUBE(projMap, IN.Tex4.xyz);

        FLOAT3 dif = (decalColor.xyz * projColor.xyz * Diffuse.xyz * IN.Color.xyz) * 2;
        FLOAT3 amb = decalColor.xyz * Ambient.xyz;
        
        // load the 3 shadow samples
        FLOAT4 shadColor0 = tex2Dproj(shadMap0, IN.Tex0.xyzw);
        FLOAT4 shadColor1 = tex2Dproj(shadMap1, IN.Tex1.xyzw);
        FLOAT4 shadColor2 = tex2Dproj(shadMap2, IN.Tex2.xyzw);
        FLOAT3 vShad = (float3)0;
        vShad.x = 1-shadColor0.b;
        vShad.y = 1-shadColor1.b;
        vShad.z = 1-shadColor2.b;
        vShad.xyz = vShad.xyz * Fading.xyz;
        FLOAT fCompare = dot(vShad.xyz, float3(1.0, 1.0, 1.0));
                
        OUT.Color.xyz = amb + dif * (1-fCompare);
        OUT.Color.w = decalColor.w * Ambient.w * IN.Color.a;
      }
      
