////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCDetailObjects_FogVolume.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D fogMap : texunit1,
                  uniform sampler2D fogEnterMap : texunit2,
                  uniform sampler2D opacityMap : texunit3,
                  uniform float4 FogColor,
                  uniform float4 Ambient }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_C0
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the opacity
        float4 opColor = tex2D(opacityMap, IN.Tex3.xy);
        // load the fog
        float4 fogColor = tex2D(fogMap, IN.Tex1.xy);
        float4 fogEnterColor = tex2D(fogEnterMap, IN.Tex2.xy);

        float3 color = (IN.Color.xyz * decalColor.xyz * opColor.xyz * Ambient.xyz) * 2;
        float fog = fogColor.a * fogEnterColor.a;
        float opac = decalColor.a * opColor.a;
        color = FogColor.xyz*fog + color*(1-fog);
        OUT.Color.xyz = color;
        OUT.Color.a = opac;
      }


