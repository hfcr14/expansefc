////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_Diff_Proj_VertAtten.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform samplerCUBE normCubeMap : texunit2,
                  uniform samplerCUBE projMap : texunit3,
                  uniform float4 Diffuse }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_C1
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);

        float4 projColor = texCUBE(projMap, IN.Tex3.xy);

        // Light vector from input color
        float4 lVec = 2*(texCUBE(normCubeMap, IN.Tex2.xyz)-0.5);
        float NdotL = saturate(dot(lVec.xyz, bumpNormal.xyz));
        float3 dif = (decalColor.xyz * NdotL * projColor.xyz * Diffuse.xyz * IN.Color1.xyz) * 2;

        // finally add them all together
        OUT.Color.xyz = dif;
        OUT.Color.w = decalColor.w * Diffuse.w;
      }

