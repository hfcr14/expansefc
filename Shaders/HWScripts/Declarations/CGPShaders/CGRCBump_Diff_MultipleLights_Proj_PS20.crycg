////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_Diff_MultipleLights_Proj_PS20.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      PS20Only

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform samplerCUBE projMap : texunit2,
                  uniform sampler2D attenMap : texunit3,
                  uniform float4 Diffuse }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_T4
          OUT_C0
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        half4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);

        half4 projColor = texCUBE(projMap, IN.Tex2.xyz);
        half atten = saturate((2*(IN.Color.b-0.5)) * -(2*(IN.Color.b-0.5)) + (1-tex2D(attenMap, IN.Tex3.xy).b));

        // normalize post-filtered bump normals
        bumpNormal.xyz = normalize(bumpNormal.xyz);
        // normalize light vector
        float3 lightVec = normalize(IN.Tex4.xyz);
        float fDif = saturate(dot(lightVec.xyz, bumpNormal.xyz));

        half3 dif = (decalColor.xyz * fDif * projColor.xyz * Diffuse.xyz * atten) * 2;

        // finally add them all together
        OUT.Color.xyz = dif;
        OUT.Color.w = decalColor.w * Diffuse.w;
      }

