// ===============================================================
// Fragment Program: Outdoor water refraction
// Description: used in outdoor refractive water
// Last Update: 29/09/2003
// Coder: Tiago Sousa
// ===============================================================


#include "../CGVPMacro.csi"


MainInput { uniform sampler2D bumpMap : texunit0,
#ifdef OPENGL
            uniform samplerRECT refMap : texunit1,             
#endif
#ifdef D3D
            uniform sampler2D refMap : texunit1,
#endif
            uniform float4 Matrix,
            uniform float4 Ambient,
            uniform float4 WaterColor
          }

DeclarationsScript
{
  struct vertout
  {
    OUT_T0
    OUT_T1
    OUT_C0
  };

  FOUT
}

CoreScript
{
  // load the decal
  float4 bumpColor = tex2D(bumpMap, IN.Tex0.xy);
  
// must create 2 versions, since offsetRECT/samplerRECT must be used for gl version...
#ifdef D3D
  # ifdef _PS_1_1
    float4 refrColor = offsettex2D(refMap, IN.Tex1.xy, bumpColor, Matrix);                 // get env bump map       
  # else
    float2 newst = IN.Tex1.xy / IN.Tex1.w;
    newst = newst.xy + Matrix.xy * bumpColor.xx + Matrix.zw * bumpColor.yy;
    float4 refrColor = tex2D(refMap, newst);
  # endif                
#endif
#ifdef OPENGL
  # ifdef _PS_1_1
    float4 refrColor = offsettexRECT(refMap, IN.Tex1.xy, bumpColor, Matrix);               // get env bump map   
  # else
    float2 newst = IN.Tex1.xy / IN.Tex1.w;
    newst = newst + Matrix.xy * bumpColor.xx + Matrix.zw * bumpColor.yy;
    float4 refrColor = texRECT(refMap, newst);
  # endif
#endif  

  OUT.Color.xyz = refrColor;                                  
  OUT.Color.w = 1;                                               
}
