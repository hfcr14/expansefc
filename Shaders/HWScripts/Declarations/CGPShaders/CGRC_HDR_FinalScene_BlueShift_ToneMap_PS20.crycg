
#include "../CGVPMacro.csi"

NoFog
PS20Only

MainInput 
{
  uniform sampler2D baseMap : texunit0,
  uniform sampler2D bloomMap : texunit1,
  uniform sampler2D starMap : texunit2,
  uniform sampler2D adaptedLumMap : texunit3,
  uniform float4 Params
}

DeclarationsScript
{
  struct vertout
  {
    OUT_T0
  };

  FOUT
}

CoreScript
{
  // The per-color weighting to be used for luminance calculations in RGB order.
  FLOAT3 LUMINANCE_VECTOR  = FLOAT3 (0.2125f, 0.7154f, 0.0721f);

  // The per-color weighting to be used for blue shift under low light.
  FLOAT3 BLUE_SHIFT_VECTOR = FLOAT3 (0.7f, 0.65f, 0.85f); 

  FLOAT LUM_WHITE = 2.0f;

  FLOAT4 vSample = tex2D(baseMap, IN.Tex0.xy);
  FLOAT4 vBloom = tex2D(bloomMap, IN.Tex0.xy);
  FLOAT4 vStar = tex2D(starMap, IN.Tex0.xy);
  FLOAT fAdaptedLum = tex2D(adaptedLumMap, float2(0.5f, 0.5f));

# if defined (_HDR_FAKE) || defined(_HDR_MRT) || defined(_HDR_ATI)
  fAdaptedLum = min(fAdaptedLum, 0.75f);
# endif

  FLOAT fStarScale = Params.x;
  FLOAT fBloomScale = Params.y;
  FLOAT fKeyValue = Params.w;

  // For very low light conditions, the rods will dominate the perception
  // of light, and therefore color will be desaturated and shifted
  // towards blue.
  // Define a linear blending from -1.5 to 2.6 (log scale) which
  // determines the lerp amount for blue shift
  //float fBlueShiftCoefficient = 1.0 - (fAdaptedLum + 1.5)/4.0;
  FLOAT fBlueShiftCoefficient = 1.0 - (fAdaptedLum + 1.5)/2.0;
  fBlueShiftCoefficient = saturate(fBlueShiftCoefficient);

  // Lerp between current color and blue, desaturated copy
  FLOAT3 vRodColor = dot(vSample.xyz, LUMINANCE_VECTOR) * BLUE_SHIFT_VECTOR;
  vSample.rgb = lerp( (float3)vSample, vRodColor, fBlueShiftCoefficient );
        
  // Map the high range of color values into a range appropriate for
  // display, taking into account the user's adaptation level, and selected
  // values for for middle gray and white cutoff.
# if defined (_HDR_FAKE) || defined(_HDR_MRT) || defined(_HDR_ATI)
  vSample.rgb *= 0.4/(fAdaptedLum + 0.001f);
  vSample.rgb *= (1.0f + vSample.rgb/LUM_WHITE);
  vSample.rgb /= 4;
# else  
  vSample.rgb *= fKeyValue/(fAdaptedLum + 0.001f);
  vSample.rgb = vSample.rgb / (1.0f + vSample.rgb/4);
# endif      

  // Add the star and bloom post processing effects
  vSample += fStarScale * vStar;
  vSample += fBloomScale * vBloom;

  OUT.Color = vSample;
}

