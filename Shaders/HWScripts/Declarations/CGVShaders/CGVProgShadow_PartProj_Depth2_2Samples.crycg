////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgShadow_Depth2_1Samples.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      VertAttributes { POSITION_3 TEXCOORD0_2 }

      NoFog      
      MainInput { VIEWPROJ_MATRIX, uniform float4x4 TexGen0, uniform float4x4 TexGen1, uniform float2x4 ShadowModelMatr0, uniform float2x4 ShadowModelMatr1, uniform float ShadowBias, LIGHT_POS, ATTEN, LIGHT_MATRIX }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_T0
          IN_C0
        };
        struct vertout
        {
          OUT_P
          OUT_T0
          OUT_T1
          OUT_T2_2
          OUT_T3
          OUT_T4
          OUT_C0
        };

      }
      PositionScript = PosCommon                 
      CoreScript
      {
        OUT.Tex2.xy = IN.TexCoord0.xy;
        OUT.Tex3 = mul(LightMatrix, vPos);
        OUT.Color = IN.Color;
        
        OUT.Tex0 = mul(TexGen0, vPos);          
        OUT.Tex1 = mul(TexGen1, vPos);          

        float fZ = dot(ShadowModelMatr0._11_12_13_14, vPos);
        float fW = dot(ShadowModelMatr0._21_22_23_24, vPos);
        OUT.Tex4.x = fZ;
        OUT.Tex4.y = fW;

        fZ = dot(ShadowModelMatr1._11_12_13_14, vPos);
        fW = dot(ShadowModelMatr1._21_22_23_24, vPos);
        OUT.Tex4.z = fZ;
        OUT.Tex4.w = fW;
      }
    