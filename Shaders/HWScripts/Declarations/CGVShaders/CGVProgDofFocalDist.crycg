////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgDofFocalDist.crycg
//  Version:     v1.00
//  Created:     16/03/2004 by Tiago Sousa.
//  Compilers:   CG/HLSL
//  Description: Depth of field effect
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////

#include "../CGVPMacro.csi"

NoFog

VertAttributes { POSITION_3 TEXCOORD0_2 }

// setup vertex components
MainInput
{
  // common model view matrix
  VIEWPROJ_MATRIX,  
  uniform float4   CameraPos,
  uniform float4   DofFocalParams
}

DeclarationsScript
{
  // vertex input
  struct appin
  {
    IN_P
    IN_T0
  };
  struct vertout
  {
    OUT_P
    OUT_T0_2
    OUT_T1
    OUT_C0
  };

}

// output vertex position
PositionScript = PosCommon

CoreScript
{
  float4 vHPos = mul(ModelViewProj, IN.Position);
  // encode in 8bits focal distance
  float fFinalDist=abs(vHPos.w-DofFocalParams.x)/DofFocalParams.y;
  

  OUT.Color.xyzw =fFinalDist;
    
  OUT.Tex0.xy = IN.TexCoord0.xy;
  
  // output planar distance  
  OUT.Tex1.xyzw= vHPos.zwzw;
  
  return OUT;
}
 