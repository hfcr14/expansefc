////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgTerrain_1Layers.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////



      #include "../CGVPMacro.csi"


      MainInput { VIEWPROJ_MATRIX, uniform float4 BaseTexGen0, uniform float4 BaseTexGen1, uniform float4 Layer1TexGen0, uniform float4 Layer1TexGen1, CAMERA_POS, uniform float4 FadingDist }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_C0
          IN_C1
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_T1_2
          OUT_C0
          OUT_C1
        };

      }
      PositionScript = PosCommon                 
      CoreScript
      {
        OUT.Tex0.x = dot(BaseTexGen0, vPos);          
        OUT.Tex0.y = dot(BaseTexGen1, vPos);          
        OUT.Tex1.x = dot(Layer1TexGen0, vPos);          
        OUT.Tex1.y = dot(Layer1TexGen1, vPos);          
        OUT.Color.a = IN.Color.a;

        float3 viewVec = CameraPos.xyz - vPos.xyz;
        float fDist = length(viewVec.xyz);
        float fFade = max(0.001, FadingDist.x);
        fDist = fDist / fFade;
        fDist = min(fDist, 1);
        fDist = fDist * fDist;
        fDist = fDist * fDist;

        OUT.Color.b = IN.Color.r * fDist;

# ifdef _HDR
        float ffCameraSpacePosZ = dot(ModelViewProj._31_32_33_34, vPos);
        OUT.Color1.xyzw = clamp((Fog.y - Fog.x*ffCameraSpacePosZ), g_VSCONST_0_025_05_1.x, g_VSCONST_0_025_05_1.w);
# endif        
      }
    