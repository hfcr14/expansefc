////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgBump_DiffSpecPass_HP_EnvCM.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"


      VertAttributes { POSITION_3 TEXCOORD0_2 TANG_3X3 }
      MainInput { VIEWPROJ_MATRIX, LIGHT_POS, CAMERA_POS, uniform float4x4 TexMatrix }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_T0
          IN_TANG
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_T1_2
          OUT_T2
          OUT_C0
          OUT_C1
        };

      }
      PositionScript = PosCommon
      CoreScript
      {
        OUT.Tex0.xy = IN.TexCoord0.xy;
        OUT.Tex1.xy = IN.TexCoord0.xy;

        TANG_MATR

        // store normalized light vector
        float3 lightVec = normalize(LightPos.xyz - vPos.xyz);

        // transform light vector from object space to tangent space and pass it as a color
        OUT.Color.xyz = 0.5 * mul(objToTangentSpace, lightVec.xyz) + 0.5.xxx;

        // compute view vector
        float3 viewVec = normalize(CameraPos.xyz - vPos.xyz);

        // compute half angle vector
        float3 halfAngleVector = normalize(lightVec.xyz + viewVec);

        // transform half angle vector from object space to tangent space and pass it as a color
        OUT.Color1.xyz = 0.5 * mul(objToTangentSpace, halfAngleVector) + 0.5.xxx;

        float3 tCamVec = viewVec;
        float3 tNormal = IN.TNormal.xyz;
        float3 tRef = dot(tNormal.xyz, tCamVec.xyz) * tNormal.xyz * 2 - tCamVec.xyz;
        float4 tRM;
        tRM.xyz = tRef.xyz;
        tRM.w = vPos.w;
        OUT.Tex2 = mul(tRM, TexMatrix);
      }
    