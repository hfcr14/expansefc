////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgBump_DiffPass_Atten_Plants_2Sided.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"


      VertAttributes { POSITION_3 TEXCOORD0_2 TANG_3X3 }
      MainInput { VIEWPROJ_MATRIX, LIGHT_POS, ATTEN }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_T0
          IN_C0
          IN_TANG
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_T1_2
          OUT_T2
          OUT_T3
          OUT_C0
          OUT_C1
        };

      }
      PositionScript = PosCommon
      CoreScript
      {
        OUT.Tex0.xy = IN.TexCoord0.xy;
        OUT.Tex1.xy = IN.TexCoord0.xy;

        // store normalized light vector
        float3 lVec = LightPos.xyz - vPos.xyz;
        float3 nlVec = normalize(lVec);

        float3 normal = IN.TNormal;
        float fDot = dot(nlVec, normal);
        normal = normal * sign(fDot);

        // compute the 3x3 tranform from tangent space to object space
        float3x3 objToTangentSpace;
        objToTangentSpace[0] = IN.Tangent;
        objToTangentSpace[1] = IN.Binormal;
        objToTangentSpace[2] = normal;

        float3 lightVec = mul(objToTangentSpace, lVec);
        
        float3 vDist = PROC_ATTENPIX;
        // transform light vector from object space to tangent space and pass it as a tex coords
        OUT.Tex3.xyz = vDist;
        OUT.Color1.xyz = vDist;
        
        OUT.Tex2.xyz = mul(objToTangentSpace, LightPos.xyz - vPos.xyz);
        OUT.Color = IN.Color;

# ifdef _HDR
        float ffCameraSpacePosZ = dot(ModelViewProj._31_32_33_34, vPos);
        OUT.Color1.w = clamp((Fog.y - Fog.x*ffCameraSpacePosZ), g_VSCONST_0_025_05_1.x, g_VSCONST_0_025_05_1.w);
# endif        
      }
    