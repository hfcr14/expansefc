; Shaders Script file
; Copyright (c) 2001-2003 Crytek Studios. All Rights Reserved.
; Author: Honich Andrey

Version (1.00)

//========================================================================
// GeForce-3

// Diffuse lighting
// At least two passes on GeForce-3 for multiple light sources
// One pass for single light source
// Supports point lights without attenuation, point lights with attenuation and projected lights

// Supports:
// 1. Dot3 light maps
// 2. Simple light maps
// 3. Three types of shadow maps (2D, Depth-maps and mixed Depth/2D)
// 4. Stencil shadows
// 5. Three types of light sources (Directional, Point/Omni, Projected)
// 6. Optimised separate techniques for Single/Multiple light sources
// 7. Optimised separate techniques for PS.2.0 shaders support (to reduce number of passes)
// 8. Env. real-time radiosity

Shader 'TemplLightDiffuse'
(
  Params
  (
    Sort = Opaque
  )
  
  #include "../LightDiffuse.csi"
)

//=========================================================================================


// Diffuse lighting
// At least two passes on GeForce-3 for multiple light sources
// One pass for single light source
// Supports point lights without attenuation, point lights with attenuation and projected lights
Shader 'TemplInvLight'
(
  Params
  (
    Sort = Opaque
    DontSortByDistance
  )
  
  // Technique 'SingleLight without LightMaps' (optimization)
  HW 'Seq'
  (
    // Diffuse lighting for projected light source
    Light
    (         
      ShadeLayer
      (
        LightType = Projected

        CGPShader = CGRCLight_InvProjSingleLight
        CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

        CGVProgram = CGVProg_LightProj_Atten
        CGVPParam ( Name = LightMatrix TranspLightMatrix )
        CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

        Layer '0'
        (
          Map = $FromLight
          TexColorOp = NoSet
  		    Blend 'DST_COLOR ZERO'        
        )                 
        Layer '1'
        (
          Map = $Attenuation2D
          ClampTexCoords
          TexColorOp = NoSet
  		    Blend 'DST_COLOR ZERO'        
        )                 
      )
    )

    // Diffuse lighting for directional light source
    // Diffuse lighting for point light source
    Light
    (         
      ShadeLayer
      (
        LightType = Directional         
        LightType = Point

        CGPShader = CGRCLight_InvSingleLight 
        CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp = 1 )

        CGVProgram = CGVProg_Light_Atten
        CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
        CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

        Layer '0'
        (
          Map = $White
          TexColorOp = NoSet
        )                 
        Layer '1'
        (
          Map = $Attenuation2D
          ClampTexCoords
          TexColorOp = NoSet
  		    Blend 'DST_COLOR ZERO'        
        )                 
      )
    )
  )
)