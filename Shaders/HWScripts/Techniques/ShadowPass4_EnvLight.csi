  //=========================================================================
  // Shadows support

Shadow
(  
#ifdef DEPTHMAPS
    ShadeLayer 
    (
      3Samples
      
      CGVProgram = CGVProgShadow_3Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = TexGen1 TempMatr1[0])
      CGVPParam (Name = TexGen2 TempMatr2[0])
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadowDepth_3Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' Comp 'REColor[1]' Comp 'REColor[2]' )

      Layer '0'
      (
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        Map = $FromRE1
        TexGen = ShadowMap
        TexColorOp = NoSet //Modulate
      )
      Layer '2'
      (
        Map = $FromRE2
        TexGen = ShadowMap
        TexColorOp = NoSet //Modulate
      )
      Layer '0'
      (
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
        Map = $Diffuse
        TexColorOp = NoSet //Modulate
        //AlphaFunc = GT0
      )
    )
    ShadeLayer 
    (
      2Samples
      
      CGVProgram = CGVProgShadow_2Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = TexGen1 TempMatr1[0])
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadowDepth_2Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' Comp 'REColor[1]' )

      Layer '0'
      (
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        Map = $FromRE1
        TexGen = ShadowMap
        TexColorOp = NoSet //Modulate
      )
      Layer '0'
      (
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
        Map = $Diffuse
        TexColorOp = NoSet //Modulate
        //AlphaFunc = GT0
      )
    )
    ShadeLayer 
    (
      1Samples
      
      CGVProgram = CGVProgShadow_1Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadowDepth_1Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' )

      Layer '0'
      (
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
        Map = $Diffuse
        TexColorOp = NoSet //Modulate
        //AlphaFunc = GT0
      )
    )

#endif

#ifdef OTHER
  #ifdef SELFSHADOW
    ShadeLayer 
    (
      3Samples
      
      CGVProgram = CGVProgShadow_Depth2_3Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = TexGen1 TempMatr1[0])
      CGVPParam (Name = TexGen2 TempMatr2[0])
      CGVPParam (Name = ShadowModelMatr0 Temp2Matr0[7])
      CGVPParam (Name = ShadowModelMatr1 Temp2Matr1[7])
      CGVPParam (Name = ShadowModelMatr2 Temp2Matr2[7])
      CGVPParam (Name = ShadowBias Comp = -0.005)
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadow_Depth2_3Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' Comp 'REColor[1]' Comp 'REColor[2]' )

      Layer '0'
      (        
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        Map = $FromRE1
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '2'
      (
        Map = $FromRE2
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '3'
      (
        Map = $Diffuse
        TexColorOp = NoSet
        AlphaFunc = GT0
      )
      Layer '4'
      (        
        Map = $AlphaGradient
        ClampTexCoords
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        TexColorOp = NoSet
      )
    )
    ShadeLayer 
    (
      2Samples
      
      CGVProgram = CGVProgShadow_Depth2_2Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = TexGen1 TempMatr1[0])
      CGVPParam (Name = ShadowModelMatr0 Temp2Matr0[7])
      CGVPParam (Name = ShadowModelMatr1 Temp2Matr1[7])
      CGVPParam (Name = ShadowBias Comp = -0.005)
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadow_Depth2_2Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' Comp 'REColor[1]' )

      Layer '0'
      (        
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        Map = $FromRE1
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '2'
      (
        Map = $Diffuse
        TexColorOp = NoSet
        AlphaFunc = GT0
      )
      Layer '3'
      (        
        Map = $AlphaGradient
        ClampTexCoords
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        TexColorOp = NoSet
      )
    )
    ShadeLayer 
    (
      1Samples
      
      CGVProgram = CGVProgShadow_Depth2_1Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = ShadowModelMatr0 Temp2Matr0[7])
      CGVPParam (Name = ShadowBias Comp = -0.005)
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadow_Depth2_1Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' )

      Layer '0'
      (        
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        Map = $Diffuse
        TexColorOp = NoSet
        AlphaFunc = GT0
      )
      Layer '2'
      (        
        Map = $AlphaGradient
        ClampTexCoords
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        TexColorOp = NoSet
      )
    )

  #endif
  #ifdef OTHER
    ShadeLayer 
    (
      3Samples
      
      CGVProgram = CGVProgShadow_3Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = TexGen1 TempMatr1[0])
      CGVPParam (Name = TexGen2 TempMatr2[0])
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadow_3Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' Comp 'REColor[1]' Comp 'REColor[2]' )

      Layer '0'
      (
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        Map = $FromRE1
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '2'
      (
        Map = $FromRE2
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '3'
      (        
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        Map = $Diffuse
        TexColorOp = NoSet
      )
    )
    ShadeLayer 
    (
      2Samples
      
      CGVProgram = CGVProgShadow_2Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = TexGen1 TempMatr1[0])
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadow_2Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' Comp 'REColor[1]' )

      Layer '0'
      (
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (
        Map = $FromRE1
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '2'
      (        
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        Map = $Diffuse
        TexColorOp = NoSet
      )
    )
    ShadeLayer 
    (
      1Samples
      
      CGVProgram = CGVProgShadow_1Samples_EnvLight
      CGVPParam (Name = TexGen0 TempMatr0[0])
      CGVPParam (Name = EnvColors EnvColor)
      CGVPParam (Name = TexMatrix TranspObjMatrix)

      CGPShader = CGRCShadow_1Samples_EnvLight
      CGPSParam (Name = Fading Comp 'REColor[0]' )

      Layer '0'
      (
        Map = $FromRE0
        TexGen = ShadowMap
        TexColorOp = NoSet
      )
      Layer '1'
      (        
        SecondPassRendState
        {
          Blend 'SRC_ALPHA ONE_MINUS_SRC_ALPHA'
          DepthFunc = Equal
          DepthWrite = 0
        }
        Map = $Diffuse
        TexColorOp = NoSet
      )
    )
  #endif
#endif  
)