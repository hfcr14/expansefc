// Diffuse lighting with Alpha glow
// for single light source

// Diffuse lighting for directional light source
Light
(         
  ShadeLayer
  (
    LightType = Directional         

    CGPShader = CGRCLight_Diff_SingleLight_AlphaGlow
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'LightColor[3]' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgLight_DiffPass
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)

    Layer '0'
    (
      Map = $Diffuse
      TexGen = Base
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
  )
)

// Diffuse lighting for projected light source
Light
(         
  ShadeLayer
  (
    LightType = Projected

    CGPShader = CGRCLight_Diff_ProjSingleLight_AlphaGlow
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'LightColor[3]' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgLight_DiffPass_Proj
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )               
    Layer '1'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
	  )                 
    Layer '2'
    (
      Map = $FromLight
      TexColorOp = NoSet
    )                 
  )
)

// Diffuse lighting for point light source
Light
(         
  ShadeLayer
  (
    LightType = Point

    CGPShader = CGRCLight_Diff_SingleLight_Atten_AlphaGlow
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'LightColor[3]' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgLight_DiffPass_Atten
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
      SecondPassRendState
      {
        DepthWrite = 0
        DepthFunc = Equal
        Blend (ONE ONE)
      }
    )                 
  )
)
