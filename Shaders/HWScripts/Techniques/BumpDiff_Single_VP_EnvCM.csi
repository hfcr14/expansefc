// Diffuse bump-mapping
// for single light source

// Diffuse lighting for directional light source
Light
(         
  /*ShadeLayer
  (
    LightType = Directional         
    NoBump

    CGPShader = CGRCBump_Diff_SingleLight_EnvLight
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_EnvLight
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = TexMatrix TranspInvObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)

    Layer '0'
    (
      Map = $Diffuse
      TexGen = Base
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '3'
    (
      Map = $ENVCMAP
      TexType = CubeMap
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )               
  )*/
  ShadeLayer
  (
    LightType = Directional         

    CGPShader = CGRCBump_Diff_SingleLight_EnvCM
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]'  Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_EnvCM
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = TexMatrix TranspInvObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '3'
    (
      Map = $ENVCMAP
      TexType = CubeMap
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )               
  )
)

// Diffuse lighting for projected light source
Light
(         
  ShadeLayer
  (
    LightType = Projected

    CGPShader = CGRCBump_Diff_ProjSingleLight_Atten_EnvCM_PS20
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_Proj_EnvCM_VS20
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = TexMatrix TranspInvObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )               
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
	  )                 
    Layer '3'
    (
      Map = $FromLight
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '5'
    (
      Map = $ENVCMAP
      TexType = CubeMap
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )               
  )
)

// Diffuse lighting for point light source
Light
(         
  ShadeLayer
  (
    LightType = Point

    CGPShader = CGRCBump_Diff_SingleLight_Atten_EnvCM_PS20
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_Atten_EnvCM_VS20
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = TexMatrix TranspInvObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '3'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $ENVCMAP
      TexType = CubeMap
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )               
  )
)
