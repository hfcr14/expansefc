# Microsoft Developer Studio Project File - Name="HWScripts" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=HWScripts - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "HWScripts.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "HWScripts.mak" CFG="HWScripts - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "HWScripts - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "HWScripts - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/MASTERCD/SHADERS/HWScripts", OHGBAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "HWScripts - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "HWScripts - Win32 Release"
# Name "HWScripts - Win32 Debug"
# Begin Group "Direct3D"

# PROP Default_Filter ""
# Begin Group "GF2 No. 1"

# PROP Default_Filter ""
# Begin Group "Declarations No. 3"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\GF2\Declarations\PShaders.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Declarations\VProgramms.csl
# End Source File
# End Group
# Begin Group "Honya No. 4"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\Direct3D\GF2\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF2\Honya\terrain.csl
# End Source File
# End Group
# End Group
# Begin Group "GF3 No. 1"

# PROP Default_Filter ""
# Begin Group "Honya No. 2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\Direct3D\GF3\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Honya\terrain.csl
# End Source File
# End Group
# Begin Group "Declarations No. 4"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\GF3\Declarations\PShaders.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\Declarations\VProgramms.csl
# End Source File
# End Group
# Begin Group "VPrograms"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBump_NOCM.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\VPrograms
ProjDir=.
InputPath=.\Direct3D\GF3\VPrograms\VProgBump_NOCM.nvv

"$(InputDir)VProgBump_NOCM.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBump_NOCM.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBumpPS.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\VPrograms
ProjDir=.
InputPath=.\Direct3D\GF3\VPrograms\VProgBumpPS.nvv

"$(InputDir)VProgBumpPS.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBumpPS.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBumpReflCM_PS.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\VPrograms
ProjDir=.
InputPath=.\Direct3D\GF3\VPrograms\VProgBumpReflCM_PS.nvv

"$(InputDir)VProgBumpReflCM_PS.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBumpReflCM_PS.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBumpRefrCM_PS.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\VPrograms
ProjDir=.
InputPath=.\Direct3D\GF3\VPrograms\VProgBumpRefrCM_PS.nvv

"$(InputDir)VProgBumpRefrCM_PS.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\VPrograms\VProgBumpRefrCM_PS.vso
# End Source File
# End Group
# Begin Group "PShaders"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\PSSpecBump.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\PShaders
ProjDir=.
InputPath=.\Direct3D\GF3\PShaders\PSSpecBump.nvp

"$(InputDir)PSSpecBump.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\PSSpecBump.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\RCBump_VP.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\PShaders
ProjDir=.
InputPath=.\Direct3D\GF3\PShaders\RCBump_VP.nvp

"$(InputDir)RCBump_VP.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\RCBump_VP.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\ReflectCMap.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\PShaders
ProjDir=.
InputPath=.\Direct3D\GF3\PShaders\ReflectCMap.nvp

"$(InputDir)ReflectCMap.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\ReflectCMap.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\RefractCMap.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\PShaders
ProjDir=.
InputPath=.\Direct3D\GF3\PShaders\RefractCMap.nvp

"$(InputDir)RefractCMap.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\RefractCMap.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\RefractCMap1.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\GF3\PShaders
ProjDir=.
InputPath=.\Direct3D\GF3\PShaders\RefractCMap1.nvp

"$(InputDir)RefractCMap1.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\GF3\PShaders\RefractCMap1.pso
# End Source File
# End Group
# End Group
# Begin Group "General"

# PROP Default_Filter ""
# Begin Group "Declarations No. 6"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\General\Declarations\PShaders.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Declarations\VProgramms.csl
# End Source File
# End Group
# Begin Group "Honya No. 7"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\Direct3D\General\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\General\Honya\terrain.csl
# End Source File
# End Group
# Begin Group "Templates No. 4"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\General\Templates\Templates.csl
# End Source File
# End Group
# End Group
# Begin Group "Radeon8500"

# PROP Default_Filter ""
# Begin Group "Declarations No. 5"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Declarations\PShaders.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Declarations\VProgramms.csl
# End Source File
# End Group
# Begin Group "Honya No. 6"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\Direct3D\Radeon8500\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\Honya\terrain.csl
# End Source File
# End Group
# Begin Group "PShaders No. 2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\PSSpecBump.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\PShaders
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\PSSpecBump.nvp

"$(InputDir)PSSpecBump.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\PSSpecBump.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCAmbient.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCAmbient.nvp

"$(ProjDir)RCAmbient.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCAmbient.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_Diff.nvp

"$(ProjDir)RCBump_Diff.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_Atten.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_Atten.nvp

"$(ProjDir)RCBump_Diff_Atten.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_Atten.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_Proj.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_Proj.nvp

"$(ProjDir)RCBump_Diff_Proj.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_Proj.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_ProjSingleLight.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_ProjSingleLight.nvp

"$(ProjDir)RCBump_Diff_ProjSingleLight.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_ProjSingleLight.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight.nvp

"$(ProjDir)RCBump_Diff_SingleLight.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight_Atten.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight_Atten.nvp

"$(ProjDir)RCBump_Diff_SingleLight_Atten.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight_Atten.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight_NOCM.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight_NOCM.nvp

"$(ProjDir)RCBump_Diff_SingleLight_NOCM.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_Diff_SingleLight_NOCM.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RCBump_DiffSpec_SingleLight.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RCBump_DiffSpec_SingleLight.nvp

"$(ProjDir)RCBump_DiffSpec_SingleLight.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\ReflectCMap.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\PShaders
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\ReflectCMap.nvp

"$(InputDir)ReflectCMap.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\ReflectCMap.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RefractCMap.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\PShaders
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RefractCMap.nvp

"$(InputDir)RefractCMap.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RefractCMap.pso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RefractCMap1.nvp

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\PShaders
ProjDir=.
InputPath=.\Direct3D\Radeon8500\PShaders\RefractCMap1.nvp

"$(InputDir)RefractCMap1.pso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\psa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\PShaders\RefractCMap1.pso
# End Source File
# End Group
# Begin Group "VPrograms No. 2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\VPrograms
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass.nvv

"$(InputDir)VProgBump_DiffPass.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass_Atten.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\VPrograms
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass_Atten.nvv

"$(InputDir)VProgBump_DiffPass_Atten.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass_Atten.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass_Proj.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\VPrograms
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass_Proj.nvv

"$(InputDir)VProgBump_DiffPass_Proj.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffPass_Proj.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffSpec.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\VPrograms
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffSpec.nvv

"$(InputDir)VProgBump_DiffSpec.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_DiffSpec.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_NOCM.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\VPrograms
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgBump_NOCM.nvv

"$(InputDir)VProgBump_NOCM.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBump_NOCM.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBumpReflCM_PS.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\VPrograms
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgBumpReflCM_PS.nvv

"$(InputDir)VProgBumpReflCM_PS.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBumpReflCM_PS.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBumpRefrCM_PS.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\Direct3D\Radeon8500\VPrograms
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgBumpRefrCM_PS.nvv

"$(InputDir)VProgBumpRefrCM_PS.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgBumpRefrCM_PS.vso
# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgSimple_Tex.nvv

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
ProjDir=.
InputPath=.\Direct3D\Radeon8500\VPrograms\VProgSimple_Tex.nvv

"$(ProjDir)VProgSimple_Tex.vso" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\vsa.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Direct3D\Radeon8500\VPrograms\VProgSimple_Tex.vso
# End Source File
# End Group
# End Group
# End Group
# Begin Group "OpenGL"

# PROP Default_Filter ""
# Begin Group "GF2"

# PROP Default_Filter ""
# Begin Group "Declarations No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\OpenGL\GF2\Declarations\PShaders.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Declarations\RCombiners.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Declarations\VProgramms.csl
# End Source File
# End Group
# Begin Group "Honya No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\OpenGL\GF2\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\Light.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF2\Honya\terrain.csl
# End Source File
# End Group
# End Group
# Begin Group "GF3"

# PROP Default_Filter ""
# Begin Group "Declarations"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\OpenGL\GF3\Declarations\PShaders.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Declarations\RCombiners.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Declarations\VProgramms.csl
# End Source File
# End Group
# Begin Group "Honya"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\OpenGL\GF3\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\AmbPass_Plants_AT_TT_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\AmbPass_Plants_AT_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\AmbPass_Plants_Bended_AT_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\AmbPass_Plants_Bended_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\AmbPass_Plants_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\AmbPass_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_First_Multiple_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Multiple_Plants_Bended_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Multiple_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Single_Plants_AT_TT_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Single_Plants_AT_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Single_Plants_Bended_AT_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Single_Plants_Bended_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Single_Plants_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiff_Single_VP.csi
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpReflLight.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\LightDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\Plants.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\GF3\Honya\terrain.csl
# End Source File
# End Group
# End Group
# Begin Group "General No. 1"

# PROP Default_Filter ""
# Begin Group "Honya No. 8"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\OpenGL\General\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\General\Honya\terrain.csl
# End Source File
# End Group
# Begin Group "Declarations No. 7"

# PROP Default_Filter ""
# End Group
# End Group
# Begin Group "Radeon8500 No. 1"

# PROP Default_Filter ""
# Begin Group "Honya No. 5"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\OpenGL\Radeon\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\LightBumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\LightBumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\OpenGL\Radeon\Honya\terrain.csl
# End Source File
# End Group
# End Group
# End Group
# Begin Group "XBox"

# PROP Default_Filter ""
# Begin Group "GF3 No. 2"

# PROP Default_Filter ""
# Begin Group "Declarations No. 2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\XBox\GF3\Declarations\PShaders.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Declarations\VProgramms.csl
# End Source File
# End Group
# Begin Group "Honya No. 3"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\XBox\GF3\Honya\Additional EdgesState.csl"
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\AlphaState.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\BumpDiffuse.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\BumpDiffuse_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\BumpOffset.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\BumpReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\BumpRefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\BumpSpecular.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\BumpSpecular_NOCM.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\DecalDetail.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\EnvirVP.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\player.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\ProjectCM.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\ReflCM.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\RefrCM.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\RefrTex.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\StencilState.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\Honya\terrain.csl
# End Source File
# End Group
# Begin Group "VPrograms No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBump_NOCM.vsh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\VPrograms
ProjDir=.
InputPath=.\XBox\GF3\VPrograms\VProgBump_NOCM.vsh

"$(InputDir)VProgBump_NOCM.xvu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBump_NOCM.xvu
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBumpPS.vsh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\VPrograms
ProjDir=.
InputPath=.\XBox\GF3\VPrograms\VProgBumpPS.vsh

"$(InputDir)VProgBumpPS.xvu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBumpPS.xvu
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBumpReflCM_PS.vsh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\VPrograms
ProjDir=.
InputPath=.\XBox\GF3\VPrograms\VProgBumpReflCM_PS.vsh

"$(InputDir)VProgBumpReflCM_PS.xvu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBumpReflCM_PS.xvu
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBumpRefrCM_PS.vsh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\VPrograms
ProjDir=.
InputPath=.\XBox\GF3\VPrograms\VProgBumpRefrCM_PS.vsh

"$(InputDir)VProgBumpRefrCM_PS.xvu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\VPrograms\VProgBumpRefrCM_PS.xvu
# End Source File
# End Group
# Begin Group "PShaders No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\PSSpecBump.psh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\PShaders
ProjDir=.
InputPath=.\XBox\GF3\PShaders\PSSpecBump.psh

"$(InputDir)PSSpecBump.xpu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\PSSpecBump.xpu
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\RCBump_VP.psh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\PShaders
ProjDir=.
InputPath=.\XBox\GF3\PShaders\RCBump_VP.psh

"$(InputDir)RCBump_VP.xpu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\RCBump_VP.xpu
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\ReflectCMap.psh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\PShaders
ProjDir=.
InputPath=.\XBox\GF3\PShaders\ReflectCMap.psh

"$(InputDir)ReflectCMap.xpu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\ReflectCMap.xpu
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\RefractCMap.psh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\PShaders
ProjDir=.
InputPath=.\XBox\GF3\PShaders\RefractCMap.psh

"$(InputDir)RefractCMap.xpu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\RefractCMap.xpu
# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\RefractCMap1.psh

!IF  "$(CFG)" == "HWScripts - Win32 Release"

!ELSEIF  "$(CFG)" == "HWScripts - Win32 Debug"

# Begin Custom Build
InputDir=.\XBox\GF3\PShaders
ProjDir=.
InputPath=.\XBox\GF3\PShaders\RefractCMap1.psh

"$(InputDir)RefractCMap1.xpu" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	$(ProjDir)\xsasm.exe $(InputPath)

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XBox\GF3\PShaders\RefractCMap1.xpu
# End Source File
# End Group
# End Group
# End Group
# Begin Group "Docs"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\Docs\Workable shader templates.doc"
# End Source File
# End Group
# Begin Source File

SOURCE=.\psa.exe
# End Source File
# Begin Source File

SOURCE=.\vsa.exe
# End Source File
# Begin Source File

SOURCE=.\xsasm.exe
# End Source File
# End Target
# End Project
